class ItemButtons5eSheet extends CONFIG.Actor.sheetClass {

	activateListeners(html) {
        super.activateListeners(html);

        // *** ItemSheetButtons5e *** Add a new item-buttons listener
        html.on('click', '.item-buttons button', ev => {
            ev.preventDefault();
            ev.stopPropagation();           

            // Extract card data
            let button = $(ev.currentTarget);
    
            // Extract action data
            let action = button.attr("data-action"),
                actor = this.actor,
                li = $(ev.currentTarget).parents(".item"),
                itemInfo = this.actor.getOwnedItem(Number(li.attr("data-item-id"))),
                itemId = itemInfo.data.id;
    
            // Get the item
            if ( !actor ) return;
            let itemData = actor.items.find(i => i.id === itemId);
            if ( !itemData ) return;
            let item = new Item5e(itemData, actor);
    
            // Weapon attack
            if ( action === "weaponAttack" ) item.rollWeaponAttack(ev);
            else if ( action === "weaponDamage" ) item.rollWeaponDamage(ev);
            else if ( action === "weaponDamage2" ) item.rollWeaponDamage(ev, true);
    
            // Spell actions
            else if ( action === "spellAttack" ) item.rollSpellAttack(ev);
            else if ( action === "spellDamage" ) item.rollSpellDamage(ev);
    
            // Feat actions
            else if ( action === "featAttack" ) item.rollFeatAttack(ev);
            else if ( action === "featDamage" ) item.rollFeatDamage(ev);
    
            // Consumable usage
            else if ( action === "consume" ) item.rollConsumable(ev);
    
            // Tool usage
            else if ( action === "toolCheck" ) item.rollToolCheck(ev);
        });


    }

    _onItemSummary(event) {
        event.preventDefault();
        let li = $(event.currentTarget).parents(".item"),
            item = this.actor.getOwnedItem(Number(li.attr("data-item-id"))),
            chatData = item.getChatData();
    
        // Toggle summary
        if ( li.hasClass("expanded") ) {
          let summary = li.children(".item-summary");
          summary.slideUp(200, () => summary.remove());
        } else {
          let div = $(`<div class="item-summary">${item.data.data.description.value}</div>`);
          
            
          // *** ItemSheetButtons5e *** Insert this new section into to the _onItemSummary method for ItemSheetButtons5e
          if (item.data.type == "weapon") {
            let buttons = $(`<div class="item-buttons"></div>`);
            buttons.append(`<span class="tag"><button data-action="weaponAttack">Attack</button></span>`);
            if (item.data.data.damage.value) buttons.append(`<span class="tag"><button data-action="weaponDamage">Damage</button></span>`);
            if (item.data.data.damage2.value) buttons.append(`<span class="tag"><button data-action="weaponDamage2">Alt. Damage</button></span>`);
            div.append(buttons);
          } else if (item.data.type == "spell") {
            let buttons = $(`<div class="item-buttons"></div>`);
            if (chatData.isSave) buttons.append(`<span class="tag">Save DC ${chatData.save.dc} (${chatData.save.str})</span>`);
            if (chatData.isAttack) buttons.append(`<span class="tag"><button data-action="spellAttack">Attack</button></span>`);
            if (item.data.data.damage.value) buttons.append(`<span class="tag"><button data-action="spellDamage">${chatData.damageLabel}</button></span>`);
            div.append(buttons);
          } else if (item.data.type == "consumable") {
            let buttons = $(`<div class="item-buttons"></div>`);
            if (chatData.hasCharges) buttons.append(`<span class="tag"><button data-action="consume">Use ${item.name}</button></span>`);
            div.append(buttons);
          } else if (item.data.type == "tool") {
            let buttons = $(`<div class="item-buttons"></div>`);
            buttons.append(`<span class="tag"><button data-action="toolCheck" data-ability="${chatData.ability.value}">Use ${item.name}</button></span>`);
            div.append(buttons);
          }
          // *** ItemSheetButtons5e *** This ends the new section.

          let props = $(`<div class="item-properties"></div>`);
          chatData.properties.forEach(p => props.append(`<span class="tag">${p}</span>`));

          div.append(props);
          li.append(div.hide());
          div.slideDown(200);
        }
        li.toggleClass("expanded");
      }
    
}

// overwriting the default Actor5eSheet sheet
CONFIG.Actor.sheetClass = ItemButtons5eSheet;