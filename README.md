# Item Sheet Buttons (5e)

This module adds buttons into the item description tag area of an item when expanded in the character sheet. This is to allow quick item interaction such as attack, damage, consume or spell DC information.

![example](preview.gif)

### Rational

My players have given me feedback on the default 5e system that they don't like spamming chat; also their planned action can get lost in chat from other peoples spam. This module adds buttons to allow players to quickly perform actions directly from their sheet.

### Current Issues:
- Currently extending the default Actor5eSheet class and overwriting the _onItemSummary method. Would prefer to use a hook and leave the 5e system alone so to avoid breaking changes in the future.

### FVTT Dependencies:
- Requires v0.2.8 of FVTT

### Installation Instructions

To install a module, follow these instructions:

1. [Download the zip file](https://gitlab.com/hooking/foundry-vtt---item-sheet-buttons/raw/master/itemsheetbuttons5e.zip) included in the module directory. If one is not provided, the module is still in early development.
2. Extract the included folder to `public/modules` in your Foundry Virtual Tabletop installation folder.
3. Restart Foundry Virtual Tabletop.  

### Feedback

If you have any suggestions or feedback, please contact me on discord (hooking#0492)